use std::process::Command;

fn main() {
    if let Ok(output) = Command::new("git").args(&["describe", "--tags"]).output() {
        if let Ok(git_hash) = String::from_utf8(output.stdout) {
            println!("cargo:rustc-env=GIT_VERSION={}", git_hash);
        }
    }
}
