/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use derive_more::Constructor;
use hashlink::{LinkedHashMap, LinkedHashSet};
use serde::{Deserialize, Serialize};

macro_rules! dassign {
    ($this:expr, $other:expr) => {
        if let Some(name) = $other {
            $this = name
        }
    };
}

macro_rules! dassign_opt {
    ($this:expr, $other:expr) => {
        if $other.is_some() {
            $this = $other.clone()
        }
    };
}

// FIXME: Separate function for floats
macro_rules! ddiff {
    ($this:expr, $other:expr) => {
        if $this != $other {
            Some($other.clone())
        } else {
            None
        }
    };
}

// By default, non-present options should resolve as a None, not a Some(None), if
// the original was a Some
// This means that the diff will exclude them, rather than including a None value that signifies
// deletion.
macro_rules! ddiff_opt {
    ($this:expr, $other:expr) => {
        if $this != $other {
            if $other.is_some() {
                Some($other.clone())
            } else {
                None
            }
        } else {
            None
        }
    };
}

/*
macro_rules! delta_record_patch {
    ($type:ty, $record_type:path, $delta_record_type:path) => {
        use crate::record::FullRecord;
        impl FullRecord for $type {
            fn get_patch(&self, master: &RecordType) -> Option<crate::record::MetaRecord> {
                use crate::record::MetaRecord;
                if let $record_type(master) = master {
                    if master != self {
                        Some(MetaRecord::Delta($delta_record_type(
                            master.create_delta(self),
                        )))
                    } else {
                        None
                    }
                } else {
                    panic!("Mispatched record types!");
                }
            }

            fn apply_patch(&mut self, other: crate::record::MetaRecord) {
                if let crate::record::MetaRecord::Delta($delta_record_type(other)) = other {
                    self.apply_delta(other);
                }
            }
        }
    };
}
*/

macro_rules! override_fullrecord {
    ($type:ty, $record_type:path, $delta_record_type:path) => {
        impl crate::record::FullRecord for $type {
            fn get_patch(
                &self,
                master: &crate::record::RecordType,
            ) -> Option<crate::record::MetaRecord> {
                use crate::delta::DeltaOf;
                use crate::record::{DeltaRecordType, MetaRecord, RecordType};
                if let $record_type(master) = master {
                    if master != self {
                        Some(MetaRecord::Delta($delta_record_type(
                            master.create_delta(self),
                        )))
                    } else {
                        None
                    }
                } else {
                    panic!(
                        "Mismatched record types for records {:?} and {:?}!",
                        self, master
                    );
                }
            }

            fn apply_patch(&mut self, other: crate::record::MetaRecord) {
                use crate::delta::DeltaOf;
                use crate::record::{DeltaRecordType, MetaRecord};
                if let MetaRecord::Delta($delta_record_type(other)) = other {
                    self.apply_delta(other);
                }
            }
        }
    };
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
#[serde(untagged)]
pub enum Deletable<T> {
    Add(T),
    Delete,
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum DeletableValue<T> {
    Add(T),
    Delete(T),
}

#[derive(Clone, Constructor, Debug, PartialEq, Serialize, Deserialize)]
pub struct Accumulator(i32);

impl Accumulator {
    pub fn add(&mut self, value: i32) {
        self.0 += value;
    }

    pub fn get(&self) -> i32 {
        self.0
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum DeltaUnion<S, DS> {
    Add(S),
    Modify(DS),
    Delete,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum DeltaUnionNoDelete<S, DS> {
    Add(S),
    Modify(DS),
}

// FIXME: We should be able to handle identifiers that are either StringIds or RecordIds
// It should only matter that one of them is present
pub trait Delta {
    fn is_empty(&self) -> bool;
}

pub trait DeltaOf<D> {
    fn create_delta(&self, new: &Self) -> D;
    fn apply_delta(&mut self, delta: D);
}

#[skip_serializing_none]
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum DeltaSet<T: Eq + std::hash::Hash> {
    Override(LinkedHashSet<T>),
    Modify {
        add: Option<LinkedHashSet<T>>,
        remove: Option<LinkedHashSet<T>>,
    },
}

pub fn create_set_scalar_diff<T: Eq + Clone + std::hash::Hash>(
    origset: &LinkedHashSet<T>,
    newset: &LinkedHashSet<T>,
) -> Option<DeltaSet<T>> {
    if origset == newset {
        None
    } else {
        let missing: LinkedHashSet<T> = origset.difference(&newset).cloned().collect();
        let added: LinkedHashSet<T> = newset.difference(&origset).cloned().collect();

        Some(DeltaSet::Modify {
            add: if added.is_empty() { None } else { Some(added) },
            remove: if missing.is_empty() {
                None
            } else {
                Some(missing)
            },
        })
    }
}

pub fn apply_set_scalar_diff<T: Clone + Eq + std::hash::Hash + std::fmt::Debug>(
    origset: &mut LinkedHashSet<T>,
    delta: Option<DeltaSet<T>>,
    context: &str,
) {
    match delta {
        Some(DeltaSet::Override(newset)) => {
            origset.clear();
            for elem in newset.into_iter() {
                origset.insert(elem);
            }
        }
        Some(DeltaSet::Modify { add, remove }) => {
            if let Some(remove) = remove {
                for elem in remove {
                    if !origset.contains(&elem) {
                        // FIXME: Context would be useful
                        warn!(
                            "{}: Delta patch attempted to remove element {:?} which doesn't exist in the set",
                            context,
                            elem
                        );
                    }
                    origset.remove(&elem);
                }
            }
            if let Some(add) = add {
                for elem in add {
                    origset.insert(elem.clone());
                }
            }
        }
        None => (),
    }
}

pub fn create_map_scalar_diff<K: PartialEq + Eq + Clone + std::hash::Hash, V: PartialEq + Clone>(
    origmap: &LinkedHashMap<K, V>,
    newmap: &LinkedHashMap<K, V>,
) -> Option<LinkedHashMap<K, Deletable<V>>> {
    if origmap == newmap {
        None
    } else {
        let missing: LinkedHashSet<K> = origmap
            .keys()
            .filter(|x| !newmap.contains_key(x))
            .cloned()
            .collect();
        let added: LinkedHashMap<K, V> = newmap
            .iter()
            .filter(|(k, v)| origmap.get(k).map_or(true, |x| &x != v))
            .map(|(k, v)| (k.clone(), v.clone()))
            .collect();

        if added.is_empty() && missing.is_empty() {
            None
        } else {
            let mut result = LinkedHashMap::new();
            for (key, value) in added.into_iter() {
                result.insert(key, Deletable::Add(value));
            }
            for key in missing {
                result.insert(key, Deletable::Delete);
            }
            Some(result)
        }
    }
}

pub fn create_map_delta_diff<
    K: PartialEq + Eq + Clone + std::hash::Hash,
    V: PartialEq + Clone + DeltaOf<DV>,
    DV: Delta,
>(
    origmap: &LinkedHashMap<K, Deletable<V>>,
    newmap: &LinkedHashMap<K, Deletable<V>>,
) -> Option<LinkedHashMap<K, DeltaUnion<V, DV>>> {
    if origmap == newmap {
        None
    } else {
        let mut result = LinkedHashMap::new();
        for (key, value) in newmap.iter() {
            match value {
                Deletable::Add(value) => {
                    if let Some(Deletable::Add(orig_value)) = origmap.get(&key) {
                        if orig_value != value {
                            let delta = orig_value.create_delta(value);
                            result.insert(key.clone(), DeltaUnion::Modify(delta));
                        }
                    } else {
                        result.insert(key.clone(), DeltaUnion::Add(value.clone()));
                    }
                }
                Deletable::Delete => {
                    result.insert(key.clone(), DeltaUnion::Delete);
                }
            }
        }
        if result.is_empty() {
            None
        } else {
            Some(result)
        }
    }
}

/// Takes two vanilla-style maps (such as for DialogeInfo, or CellRef,
/// and determines the difference between them in a way which can be Serialized
/// to an esplugin.
/// The inputs should be the entire maps, including all the entries added by masters
pub fn create_map_vanilla_diff<
    K: PartialEq + Eq + Clone + std::hash::Hash,
    V: PartialEq + Clone,
>(
    origmap: &LinkedHashMap<K, Deletable<V>>,
    newmap: &LinkedHashMap<K, Deletable<V>>,
) -> LinkedHashMap<K, Deletable<V>> {
    let mut result = LinkedHashMap::new();

    for (key, value) in origmap.iter() {
        if !newmap.contains_key(key) && value != &Deletable::Delete {
            result.insert(key.clone(), Deletable::Delete);
        }
    }
    for (key, value) in newmap.iter() {
        match value {
            Deletable::Add(value) => {
                if let Some(Deletable::Add(orig_value)) = origmap.get(&key) {
                    if orig_value != value {
                        result.insert(key.clone(), Deletable::Add(value.clone()));
                    }
                } else {
                    result.insert(key.clone(), Deletable::Add(value.clone()));
                }
            }
            Deletable::Delete => {
                if let Some(Deletable::Delete) = origmap.get(&key) {
                } else {
                    result.insert(key.clone(), Deletable::Delete);
                }
            }
        }
    }

    result
}

pub fn apply_map_scalar_diff<
    K: Clone + Eq + std::hash::Hash + std::fmt::Display,
    V: PartialEq + Clone,
>(
    origmap: &mut LinkedHashMap<K, V>,
    delta: Option<LinkedHashMap<K, Deletable<V>>>,
    context: &str,
) {
    if let Some(delta) = delta {
        for (key, value) in delta.into_iter() {
            match value {
                Deletable::Add(value) => {
                    origmap.insert(key, value);
                }
                Deletable::Delete => {
                    if origmap.remove(&key).is_none() {
                        warn!(
                            "{}: Delta patch attempted to remove element {} which doesn't exist in the map",
                            context,
                            key
                        );
                    }
                }
            }
        }
    }
}

pub fn apply_map_delta_diff<
    K: Clone + Eq + std::hash::Hash + std::fmt::Display,
    V: PartialEq + Clone + DeltaOf<DV>,
    DV,
>(
    origmap: &mut LinkedHashMap<K, Deletable<V>>,
    delta: Option<LinkedHashMap<K, DeltaUnion<V, DV>>>,
    context: &str,
) {
    if let Some(delta) = delta {
        for (key, value) in delta.into_iter() {
            match value {
                DeltaUnion::Add(value) => {
                    origmap.insert(key, Deletable::Add(value));
                }
                DeltaUnion::Modify(value) => {
                    if let Some(Deletable::Add(orig_value)) = origmap.get_mut(&key) {
                        orig_value.apply_delta(value);
                    } else {
                        warn!(
                            "{}: Delta patch attempted to modify element {} which doesn't exist",
                            context, key
                        )
                    }
                }
                DeltaUnion::Delete => {
                    if origmap.remove(&key).is_none() {
                        warn!(
                            "{}: Delta patch attempted to remove element {} which doesn't exist in the map",
                            context,
                            key
                        );
                    }
                }
            }
        }
    }
}

pub fn diff_accumulators<K: PartialEq + Eq + Clone + std::hash::Hash>(
    origmap: &LinkedHashMap<K, Accumulator>,
    newmap: &LinkedHashMap<K, Accumulator>,
) -> Option<LinkedHashMap<K, Deletable<Accumulator>>> {
    if origmap == newmap {
        None
    } else {
        let mut result = LinkedHashMap::new();
        for (key, value) in newmap {
            if let Some(origvalue) = origmap.get(key) {
                if value.get() != origvalue.get() {
                    result.insert(
                        key.clone(),
                        Deletable::Add(Accumulator::new(value.get() - origvalue.get())),
                    );
                }
            } else {
                result.insert(key.clone(), Deletable::Add(value.clone()));
            }
        }

        for key in origmap.keys() {
            if !newmap.contains_key(&key) {
                result.insert(key.clone(), Deletable::Delete);
            }
        }
        if result.is_empty() {
            None
        } else {
            Some(result)
        }
    }
}

pub fn apply_accumulator<K: Clone + Eq + std::hash::Hash + std::fmt::Display>(
    origmap: &mut LinkedHashMap<K, Accumulator>,
    delta: Option<LinkedHashMap<K, Deletable<Accumulator>>>,
    context: &str,
) {
    if let Some(delta) = delta {
        for (key, value) in delta.into_iter() {
            match value {
                Deletable::Add(value) => {
                    if let Some(orig) = origmap.get_mut(&key) {
                        orig.add(value.get());
                    } else {
                        origmap.insert(key, value);
                    }
                }
                Deletable::Delete => {
                    if origmap.remove(&key).is_none() {
                        warn!(
                            "{}: Delta patch attempted to remove element {} which doesn't exist in the map",
                            context,
                            key
                        );
                    }
                }
            }
        }
    }
}

pub fn create_multiset_diff<V: Clone + Eq + std::hash::Hash>(
    origset: &Vec<V>,
    newset: &Vec<V>,
) -> Option<Vec<DeletableValue<V>>> {
    let orig_values: LinkedHashSet<&V> = origset.iter().collect();
    let mut diff = vec![];
    for value in newset {
        if !orig_values.contains(value) {
            diff.push(DeletableValue::Add(value.clone()));
        }
    }
    let new_values: LinkedHashSet<&V> = newset.iter().collect();
    for value in origset {
        if !new_values.contains(value) {
            diff.push(DeletableValue::Delete(value.clone()));
        }
    }
    if diff.is_empty() {
        None
    } else {
        Some(diff)
    }
}

pub fn apply_multiset_diff<V: Clone + Eq + std::hash::Hash>(
    origset: &mut Vec<V>,
    delta: Option<Vec<DeletableValue<V>>>,
) {
    if let Some(delta) = delta {
        for value in delta {
            match value {
                DeletableValue::Add(value) => {
                    origset.push(value);
                }
                DeletableValue::Delete(value) => {
                    for (index, elem) in origset.iter().enumerate() {
                        if elem == &value {
                            origset.remove(index);
                            // Only remove one
                            break;
                        }
                    }
                }
            }
        }
    }
}
