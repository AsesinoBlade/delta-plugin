/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::nom_derive::Parse;
use crate::record::RecordPair;
use crate::types::{AttributeType, SkillType, SpecializationType};
use crate::util::{push_opt_str_subrecord, subrecords_len, take_nt_str, RecordError};
use derive_more::Display;
use hashlink::LinkedHashMap;
use nom::number::complete::le_i32;
use num_derive::FromPrimitive;
use num_traits::FromPrimitive;
use serde::{Deserialize, Serialize};
use serde_repr::{Deserialize_repr, Serialize_repr};
use std::convert::{TryFrom, TryInto};

/// Enum representing the numeric indices of the use values
#[derive(
    Clone, Debug, Hash, Eq, PartialEq, FromPrimitive, Display, Serialize_repr, Deserialize_repr,
)]
#[repr(u8)]
pub enum UseValue {
    Zero = 0,
    One = 1,
    Two = 2,
    Three = 3,
}

#[skip_serializing_none]
#[derive(DeltaRecord, FullRecord, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Skill {
    attribute: AttributeType,
    specialization: SpecializationType,
    use_values: LinkedHashMap<UseValue, f32>,
    desc: Option<String>,
}

impl TryFrom<esplugin::Record> for RecordPair<Skill> {
    type Error = RecordError;

    fn try_from(record: esplugin::Record) -> Result<Self, Self::Error> {
        let subrecords = record.subrecords();

        let mut skill: Option<SkillType> = None;
        let mut desc = None;

        #[derive(Nom)]
        #[nom(LittleEndian)]
        struct Data {
            attribute: i32,
            specialization: i32,
            #[nom(Count = "4")]
            use_values: Vec<f32>,
        }

        let mut data_struct: Option<Data> = None;

        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"INDX" => {
                    let tmp = le_i32(data)?.1;
                    skill =
                        Some(FromPrimitive::from_i32(tmp).ok_or(RecordError::InvalidSkillId(tmp))?);
                }
                b"DESC" => desc = Some(take_nt_str(data)?.1),
                b"SKDT" => data_struct = Some(Data::parse(data)?.1),
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }

        let skill = skill.ok_or(RecordError::MissingSubrecord("INDX"))?;
        let data = data_struct.ok_or(RecordError::MissingSubrecord("SKDT"))?;

        Ok(RecordPair::new(
            skill.into(),
            Skill {
                attribute: FromPrimitive::from_i32(data.attribute)
                    .ok_or(RecordError::InvalidAttributeId(data.attribute))?,
                specialization: FromPrimitive::from_i32(data.specialization)
                    .ok_or(RecordError::InvalidSpecId(data.specialization))?,
                use_values: data
                    .use_values
                    .into_iter()
                    .enumerate()
                    .map(|(index, value)| {
                        (
                            FromPrimitive::from_usize(index)
                                //.ok_or(RecordError::InvalidSkillUseValueIndex(index, skill))?
                                .unwrap(),
                            value,
                        )
                    })
                    .collect(),
                desc,
            },
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Skill> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<esplugin::Subrecord> = vec![];
        let id: SkillType = self.id.try_into()?;

        subrecords.push(esplugin::Subrecord::new(
            *b"INDX",
            (id as u32).to_le_bytes().to_vec(),
            false,
        ));
        let mut data = vec![];
        data.extend(&(self.record.attribute as u32).to_le_bytes());
        data.extend(&(self.record.specialization as u32).to_le_bytes());
        let mut use_values = [0f32; 4];
        for (index, use_value) in self.record.use_values.into_iter() {
            use_values[index as usize] = use_value;
        }
        for value in &use_values {
            data.extend(&value.to_le_bytes());
        }
        subrecords.push(esplugin::Subrecord::new(*b"SKDT", data, false));
        push_opt_str_subrecord(&mut subrecords, &self.record.desc, "DESC")?;

        let header = esplugin::RecordHeader::new(*b"SKIL", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
