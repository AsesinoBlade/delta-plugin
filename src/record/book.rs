/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::types::SkillType;
use crate::util::{
    parse_float, push_opt_str_subrecord, push_str_subrecord, subrecords_len, take_nt_str,
    RecordError,
};
use derive_more::Constructor;
use esplugin::{RecordHeader, Subrecord};
use nom::number::complete::le_f32;
use nom_derive::Parse;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[skip_serializing_none]
#[derive(DeltaRecord, FullRecord, Constructor, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Book {
    /// In-game name for the Book
    pub name: Option<String>,
    /// Model identifier
    pub model: Option<String>,
    /// Icon identifier
    pub icon: Option<String>,
    /// Weight of the Book
    pub weight: f64,
    /// Value of the Book
    pub value: i64,
    /// Whether or not this book is a scroll
    // FIXME: There's probably a better way to handle scrolls than using a flag.
    // E.g. have them be separate types that are otherwise identical (an enum could be used to
    // avoid duplication of information, however we would need the delta macro to be able to handle
    // enums)
    pub scroll: bool,
    /// One-time permanent Skill increase provided by this book when read
    pub skill: Option<SkillType>,
    /// Maximum enchantment points the book can be enchanted with?
    pub enchantment_points: i64,
    /// Text contents of the book (HTML? FIXME: More details on format?)
    // FIXME: Text should be serialized to a separate file by default
    pub text: Option<String>,
    /// Script (FIXME: When is the script run?)
    pub script: Option<String>,
    /// Enchantment
    pub enchantment: Option<String>,
}

impl TryFrom<esplugin::Record> for RecordPair<Book> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<RecordPair<Book>, RecordError> {
        let subrecords = record.subrecords();
        let mut id = None;
        let mut name = None;
        let mut model = None;
        let mut script = None;
        let mut icon = None;
        let mut text = None;
        let mut enchantment = None;
        let mut book_data = None;

        #[derive(Nom)]
        #[nom(LittleEndian)]
        struct Data {
            #[nom(Parse = "le_f32")]
            weight: f32,
            value: i32,
            scroll: i32,
            skill: i32,
            enchantment_points: i32,
        }

        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"FNAM" => name = Some(take_nt_str(data)?.1),
                b"MODL" => model = Some(take_nt_str(data)?.1),
                b"SCRI" => script = Some(take_nt_str(data)?.1),
                b"ITEX" => icon = Some(take_nt_str(data)?.1),
                b"TEXT" => text = Some(take_nt_str(data)?.1),
                b"ENAM" => enchantment = Some(take_nt_str(data)?.1),
                b"BKDT" => {
                    book_data = Some(Data::parse(data)?.1);
                }
                b"DELE" => (),
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }

        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;
        let data = book_data.ok_or(RecordError::MissingSubrecord("BKDT"))?;
        let skill = if data.skill < 0 {
            None
        } else {
            Some(data.skill.try_into()?)
        };
        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::Book, id),
            Book::new(
                name,
                model,
                icon,
                parse_float(data.weight),
                data.value as i64,
                data.scroll != 0,
                skill,
                data.enchantment_points as i64,
                text,
                script,
                enchantment,
            ),
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Book> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        push_str_subrecord(&mut subrecords, &id, "NAME")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.name, "FNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.model, "MODL")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.icon, "ITEX")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.script, "SCRI")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.text, "TEXT")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.enchantment, "ENAM")?;

        let mut data = vec![];
        data.extend(&(self.record.weight as f32).to_le_bytes());
        data.extend(&(self.record.value as i32).to_le_bytes());
        data.extend(&(self.record.scroll as i32).to_le_bytes());
        data.extend(
            &self
                .record
                .skill
                .map(|x| x as i32)
                .unwrap_or(-1)
                .to_le_bytes(),
        );
        data.extend(&(self.record.enchantment_points as i32).to_le_bytes());
        subrecords.push(Subrecord::new(*b"BKDT", data, false));

        let header = RecordHeader::new(*b"BOOK", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
