/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::plugin::Plugin;
use crate::util::{
    nt_str, record_type, subrecords_len, take_nt_str, take_nt_str_exact, RecordError,
};
use crate::version::get_version;
use esplugin::{Record, RecordHeader, Subrecord};
use nom::number::complete::{le_f32, le_i32, le_u64};
use semver::Version;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

// TODO: Maybe store a hash to ensure that version numbers are accurate?
#[skip_serializing_none]
#[derive(Clone, Eq, PartialEq, Hash, Debug, Serialize, Deserialize)]
pub struct Master {
    pub name: String,
    pub size: u64,
    pub version: Option<Version>,
}

impl Master {
    pub fn new(plugin: &Plugin) -> Result<Self, RecordError> {
        let size = std::fs::metadata(&plugin.file)?.len();
        Ok(Master {
            name: plugin
                .file
                .as_path()
                .file_name()
                .unwrap()
                .to_str()
                .unwrap()
                .to_string(),
            size,
            version: plugin.header.version.clone(),
        })
    }
}

#[skip_serializing_none]
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Header {
    // We support reading any version, but only really support writing version 1.3
    __esp_version__: Option<f64>,
    __plugin_type__: Option<i32>, // 0=esp, 1=esm, 32=ess (unused)
    pub transpiler_version: Option<Version>,
    pub version: Option<Version>,
    pub author: String, // Author's name
    pub desc: String,   // File description
    // TODO: Master sizes could be generated when converting to esp
    // We could embed plugin version information in the header, and version masters instead
    pub masters: Vec<Master>,
    #[serde(skip)]
    num_records: Option<usize>,
}

impl Header {
    pub fn clear_private(&mut self) {
        self.__esp_version__ = None;
        self.__plugin_type__ = None;
    }

    pub fn set_num_records(&mut self, num_records: usize) {
        self.num_records = Some(num_records);
    }

    pub fn push_masters_context(&self) {
        crate::plugin::PLUGINS.with(|v| {
            let mut v = v.borrow_mut();
            for master in &self.masters {
                v.push(
                    std::path::Path::new(&master.name)
                        .file_stem()
                        .unwrap()
                        .to_os_string()
                        .into_string()
                        .unwrap()
                        .to_lowercase(),
                );
            }
        });
    }
}

impl Default for Header {
    fn default() -> Self {
        Header {
            __esp_version__: None,
            __plugin_type__: None,
            transpiler_version: Some(get_version()),
            version: None,
            author: format!(""),
            desc: format!(""),
            masters: vec![],
            num_records: None,
        }
    }
}

impl TryFrom<&Record> for Header {
    type Error = RecordError;
    fn try_from(record: &Record) -> Result<Header, RecordError> {
        let subrecords = record.subrecords();
        let data = subrecords[0].data();
        let (data, version) = le_f32(data)?;
        let (data, plugin_type) = le_i32(data)?;
        let (data, author) = take_nt_str_exact(&data, 32)?;
        let (_, desc) = take_nt_str_exact(&data, 256)?;
        let mut masters = vec![];

        let (s_masters, s_sizes): (Vec<&Subrecord>, Vec<&Subrecord>) = subrecords[1..]
            .iter()
            .partition(|x| x.subrecord_type() == b"MAST");
        for (sub1, sub2) in s_masters.iter().zip(s_sizes.iter()) {
            let (_, master) = take_nt_str(&sub1.data())?;
            let (_, size) = le_u64(sub2.data())?;

            // Store masters in thread-local storage so that they can be accessed
            // to determine plugin names referenced in CellRefIds
            crate::plugin::PLUGINS.with(|v| {
                v.borrow_mut().push(
                    std::path::Path::new(&master)
                        .file_stem()
                        .unwrap()
                        .to_os_string()
                        .into_string()
                        .unwrap()
                        .to_lowercase(),
                );
            });

            masters.push(Master {
                name: master,
                size,
                version: None,
            })
        }

        Ok(Header {
            __esp_version__: Some(version.to_string().parse().unwrap()),
            __plugin_type__: Some(plugin_type),
            transpiler_version: Some(get_version()),
            version: None,
            author,
            desc,
            masters,
            num_records: None,
        })
    }
}

impl TryInto<Record> for Header {
    type Error = RecordError;

    fn try_into(self) -> Result<Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];

        let mut data = vec![];
        data.extend(1.3f32.to_le_bytes().iter());
        data.extend(vec![0, 0, 0, 0]);
        data.extend(nt_str(&self.author)?);
        data.resize(40, Default::default()); // Author string is max 32 bytes
        data.extend(nt_str(&self.desc)?);
        data.resize(296, Default::default()); // Author string is max 256 bytes
        data.extend((self.num_records.unwrap() as u32).to_le_bytes().iter());
        subrecords.push(Subrecord::new(*b"HEDR", data, false));
        for master in self.masters {
            let name = nt_str(&master.name)?;
            subrecords.push(Subrecord::new(*b"MAST", name, false));
            subrecords.push(Subrecord::new(
                *b"DATA",
                master.size.to_le_bytes().to_vec(),
                false,
            ));
        }

        let header = RecordHeader::new(record_type("TES3"), 0, None, subrecords_len(&subrecords));

        Ok(Record::new(header, subrecords))
    }
}
