/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{Pair, RecordId, RecordPair, RecordTypeName};
use crate::util::{
    parse_float, push_opt_str_subrecord, push_str_subrecord, subrecords_len, take_nt_str,
    RecordError,
};
use derive_more::{Constructor, Display};
use esplugin::{RecordHeader, Subrecord};
use hashlink::LinkedHashMap;
use nom::number::complete::{le_f32, le_u8};
use nom_derive::Parse;
use num_traits::FromPrimitive;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[derive(
    Display, Copy, Clone, Debug, Eq, PartialEq, Serialize, Deserialize, FromPrimitive, Hash,
)]
pub enum BodyPartReferenceType {
    Head = 0,
    Hair = 1,
    Neck = 2,
    Cuirass = 3,
    Groin = 4,
    Skirt = 5,
    RightHand = 6,
    LeftHand = 7,
    RightWrist = 8,
    LeftWrist = 9,
    Shield = 10,
    RightForearm = 11,
    LeftForearm = 12,
    RightUpperArm = 13,
    LeftUpperArm = 14,
    RightFoot = 15,
    LeftFoot = 16,
    RightAnkle = 17,
    LeftAnkle = 18,
    RightKnee = 19,
    LeftKnee = 20,
    RightLeg = 21,
    LeftLeg = 22,
    RightPauldron = 23,
    LeftPauldron = 24,
    Weapon = 25,
    Tail = 26,
}

/// FIXME: What is this used for?
#[skip_serializing_none]
#[derive(Clone, Constructor, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BodyPartReference {
    pub male_name: Option<String>,
    pub female_name: Option<String>,
}

impl TryInto<Vec<Subrecord>> for Pair<BodyPartReferenceType, BodyPartReference> {
    type Error = RecordError;

    fn try_into(self) -> Result<Vec<Subrecord>, RecordError> {
        let mut subrecords = vec![];
        subrecords.push(Subrecord::new(
            *b"INDX",
            (self.id as u8).to_le_bytes().to_vec(),
            false,
        ));
        push_opt_str_subrecord(&mut subrecords, &self.value.male_name, "BNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.value.female_name, "CNAM")?;
        Ok(subrecords)
    }
}

#[derive(Display, Copy, Clone, Debug, Eq, PartialEq, Serialize, Deserialize, FromPrimitive)]
pub enum ArmourType {
    Helmet = 0,
    Cuirass = 1,
    LeftPauldron = 2,
    RightPauldron = 3,
    Greaves = 4,
    Boots = 5,
    LeftGauntlet = 6,
    RightGauntlet = 7,
    Shield = 8,
    LeftBracer = 9,
    RightBracer = 10,
}

#[skip_serializing_none]
#[derive(DeltaRecord, FullRecord, Constructor, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Armour {
    /// In-game name for the Armour
    pub name: Option<String>,
    /// Model identifier
    pub model: Option<String>,
    /// Icon identifier
    pub icon: Option<String>,
    /// Type of Armour
    pub armour_type: ArmourType,
    /// Weight of the Armour
    pub weight: f64,
    /// Value of the Armour
    pub value: i64,
    /// Health of the Armour
    pub health: i64,
    /// Enchantment Points for enchanting the armour
    pub enchantment_points: i64,
    /// Armour Rating
    pub rating: i64,
    /// FIXME: What is this used for?
    pub body_parts: LinkedHashMap<BodyPartReferenceType, BodyPartReference>,
    /// Script (FIXME: When is the script run?)
    pub script: Option<String>,
    /// Armour's Enchantment
    pub enchant: Option<String>,
}

impl TryFrom<esplugin::Record> for RecordPair<Armour> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<RecordPair<Armour>, RecordError> {
        let subrecords = record.subrecords();
        let mut id = None;
        let mut name = None;
        let mut model = None;
        let mut script = None;
        let mut icon = None;
        let mut enchant = None;
        let mut armour_data = None;
        let mut body_parts = LinkedHashMap::new();
        let mut last = None;

        #[derive(Nom)]
        #[nom(LittleEndian)]
        struct Data {
            armour_type: i32,
            #[nom(Parse = "le_f32")]
            weight: f32,
            value: i32,
            health: i32,
            enchant: i32,
            armour: i32,
        }

        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"FNAM" => name = Some(take_nt_str(data)?.1),
                b"MODL" => model = Some(take_nt_str(data)?.1),
                b"SCRI" => script = Some(take_nt_str(data)?.1),
                b"ITEX" => icon = Some(take_nt_str(data)?.1),
                b"ENAM" => enchant = Some(take_nt_str(data)?.1),
                b"AODT" => {
                    armour_data = Some(Data::parse(data)?.1);
                }
                b"DELE" => (),
                b"INDX" => {
                    let body_part = le_u8(data)?.1;
                    let typ = FromPrimitive::from_u8(body_part)
                        .ok_or(RecordError::InvalidBodyPart(body_part))?;
                    last = Some(typ);
                    body_parts.insert(typ, BodyPartReference::new(None, None));
                }
                b"BNAM" => {
                    body_parts
                        .get_mut(&last.ok_or(RecordError::MissingSubrecord("INDX"))?)
                        .unwrap()
                        .male_name = Some(take_nt_str(data)?.1);
                }
                b"CNAM" => {
                    body_parts
                        .get_mut(&last.ok_or(RecordError::MissingSubrecord("INDX"))?)
                        .unwrap()
                        .female_name = Some(take_nt_str(data)?.1);
                }
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }

        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;
        let data = armour_data.ok_or(RecordError::MissingSubrecord("AODT"))?;
        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::Armour, id),
            Armour::new(
                name,
                model,
                icon,
                FromPrimitive::from_i32(data.armour_type)
                    .ok_or(RecordError::InvalidArmourType(data.armour_type))?,
                parse_float(data.weight),
                data.value as i64,
                data.health as i64,
                data.enchant as i64,
                data.armour as i64,
                body_parts,
                script,
                enchant,
            ),
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Armour> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        push_str_subrecord(&mut subrecords, &id, "NAME")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.name, "FNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.model, "MODL")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.icon, "ITEX")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.script, "SCRI")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.enchant, "ENAM")?;

        let mut data = vec![];
        data.extend(&(self.record.armour_type as i32).to_le_bytes());
        data.extend(&(self.record.weight as f32).to_le_bytes());
        data.extend(&(self.record.value as i32).to_le_bytes());
        data.extend(&(self.record.health as i32).to_le_bytes());
        data.extend(&(self.record.enchantment_points as i32).to_le_bytes());
        data.extend(&(self.record.rating as i32).to_le_bytes());
        subrecords.push(Subrecord::new(*b"AODT", data, false));

        for (typ, body_part) in self.record.body_parts {
            let subs: Vec<Subrecord> = Pair::new(typ, body_part).try_into()?;
            subrecords.extend(subs);
        }

        let header = RecordHeader::new(*b"ARMO", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
