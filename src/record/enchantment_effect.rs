/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::effect::Effect;
use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::util::{push_str_subrecord, subrecords_len, take_nt_str, RecordError};
use derive_more::Constructor;
use esplugin::{RecordHeader, Subrecord};
use nom::number::complete::le_i32;
use num_traits::FromPrimitive;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[derive(Copy, Clone, Debug, Eq, FromPrimitive, PartialEq, Serialize, Deserialize)]
pub enum EnchantmentType {
    CastOnce = 0,
    CastOnStrike = 1,
    CastOnUse = 2,
    ConstantEffect = 3,
}

/// Enchantment Effects
#[skip_serializing_none]
#[derive(DeltaRecord, FullRecord, Constructor, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct EnchantmentEffect {
    /// The Type of enchantment
    enchantment_type: EnchantmentType,
    /// Cost in gold to create
    cost: u64,
    /// Charge used when casting
    charge: u64,
    /// AutoCalc (FIXME: What does this do?)
    autocalc: bool,
    /// Effects on use
    /// FIXME: How are duplicate effects handled?
    /// Could you get the same result by combining them somehow?
    /// The main difference is that multiple effects, even with the same range of magnitudes
    /// will have a different deviation, but the deviation could also be included.
    #[multiset]
    effects: Vec<Effect>,
}

impl TryFrom<esplugin::Record> for RecordPair<EnchantmentEffect> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let subrecords = record.subrecords();
        let mut id = None;
        let mut enchantment_type = None;
        let mut cost = None;
        let mut charge = None;
        let mut autocalc = None;
        let mut effects = vec![];
        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"ENAM" => {
                    let effect: Result<Effect, RecordError> = data.try_into();
                    match effect {
                        Ok(effect) => {
                            effects.push(effect);
                        }
                        Err(err) => {
                            error!(
                                "Skipping Effect in enchantment \"{}\"\n\nCaused by:\n    {}",
                                id.as_ref().unwrap_or(&"<unknown>".to_string()),
                                err
                            );
                        }
                    }
                }
                b"ENDT" => {
                    let (data, _type) = le_i32(data)?;
                    let (data, _cost) = le_i32(data)?;
                    let (data, _charge) = le_i32(data)?;
                    let (_, _autocalc) = le_i32(data)?;
                    enchantment_type = Some(
                        FromPrimitive::from_i32(_type)
                            .ok_or(RecordError::InvalidEnchantmentType(_type))?,
                    );
                    cost = Some(_cost as u64);
                    charge = Some(_charge as u64);
                    // FIXME: There is also a 0xFFFE flag in Morrowind, with unknown meaning
                    // OpenMW lists it as "Invalid"
                    autocalc = Some(_autocalc == 1);
                }
                b"DELE" => (),
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }
        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;
        let enchantment_type = enchantment_type.ok_or(RecordError::MissingSubrecord("ENDT"))?;
        let cost = cost.ok_or(RecordError::MissingSubrecord("ENDT"))?;
        let charge = charge.ok_or(RecordError::MissingSubrecord("ENDT"))?;
        let autocalc = autocalc.ok_or(RecordError::MissingSubrecord("ENDT"))?;
        let effect = EnchantmentEffect::new(enchantment_type, cost, charge, autocalc, effects);
        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::EnchantmentEffect, id),
            effect,
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<EnchantmentEffect> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        push_str_subrecord(&mut subrecords, &id, "NAME")?;

        let mut data = vec![];
        data.extend(&(self.record.enchantment_type as i32).to_le_bytes());
        data.extend(&(self.record.cost as i32).to_le_bytes());
        data.extend(&(self.record.charge as i32).to_le_bytes());
        data.extend(&(self.record.autocalc as i32).to_le_bytes());
        subrecords.push(Subrecord::new(*b"ENDT", data, false));

        for effect in self.record.effects {
            subrecords.push(effect.into());
        }

        let header = RecordHeader::new(*b"ENCH", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
