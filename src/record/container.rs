/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::util::{
    is_default, nt_str, parse_float, push_opt_str_subrecord, push_str_subrecord, subrecords_len,
    take_nt_str, RecordError,
};
use derive_more::{Constructor, Display};
use enumflags2::BitFlags;
use esplugin::{RecordHeader, Subrecord};
use hashlink::{LinkedHashMap, LinkedHashSet};
use nom::number::complete::{le_f32, le_i32, le_u32};
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[bitflags]
#[derive(Display, Copy, Clone, Debug, Eq, PartialEq, Hash, Serialize, Deserialize)]
#[repr(u32)]
pub enum ContainerTraits {
    /// The container is organic (e.g. corpse)
    Organic = 1,
    /// The contents of the container respawn
    /// Respawn period is 4 months
    Respawns = 2,
    /// FIXME: Unknown
    Unknown = 8,
}

#[skip_serializing_none]
#[derive(DeltaRecord, FullRecord, Constructor, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Container {
    /// In-game name of the container
    pub name: Option<String>,
    /// Identifier for the container's model
    pub model: Option<String>,
    /// Weight of the container
    pub weight: f64,
    #[serde(default, skip_serializing_if = "is_default")]
    pub traits: LinkedHashSet<ContainerTraits>,
    /// A mapping of item identifiers to their quantity in the container
    /// Negative values are restockable
    /// FIXME: This should be specified separately so that it is more transparent.
    #[serde(default, skip_serializing_if = "is_default")]
    pub contents: LinkedHashMap<String, i64>,
    /// A script to run on container interaction (FIXME: Is this correct?)
    pub script: Option<String>,
}

impl TryFrom<esplugin::Record> for RecordPair<Container> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<RecordPair<Container>, RecordError> {
        let subrecords = record.subrecords();
        let mut id = None;
        let mut name = None;
        let mut model = None;
        let mut weight = None;
        let mut traits = LinkedHashSet::new();
        let mut contents = LinkedHashMap::new();
        let mut script = None;

        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"FNAM" => name = Some(take_nt_str(data)?.1),
                b"MODL" => model = Some(take_nt_str(data)?.1),
                b"CNDT" => {
                    weight = Some(parse_float(le_f32(data)?.1));
                }
                b"FLAG" => {
                    let flags = le_u32(data)?.1;
                    traits = BitFlags::<ContainerTraits>::from_bits(flags)?
                        .iter()
                        .collect();
                }
                b"NPCO" => {
                    let (data, count) = le_i32(data)?;
                    let (_, name) = take_nt_str(data)?;
                    contents.insert(name, count as i64);
                }
                b"SCRI" => script = Some(take_nt_str(data)?.1),
                b"DELE" => (),
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }

        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;
        let weight = weight.ok_or(RecordError::MissingSubrecord("CNDT"))?;

        let container = Container::new(name, model, weight, traits, contents, script);

        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::Container, id),
            container,
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Container> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];

        let id: String = self.id.try_into()?;

        push_str_subrecord(&mut subrecords, &id, "NAME")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.name, "FNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.model, "MODL")?;
        subrecords.push(Subrecord::new(
            *b"CNDT",
            (self.record.weight as f32).to_le_bytes().to_vec(),
            false,
        ));
        let mut flags = 0u32;
        for flag in self.record.traits {
            flags |= flag as u32;
        }
        subrecords.push(Subrecord::new(
            *b"FLAG",
            flags.to_le_bytes().to_vec(),
            false,
        ));
        push_opt_str_subrecord(&mut subrecords, &self.record.script, "SCRI")?;
        for (item, count) in self.record.contents {
            let mut data = vec![];
            data.extend((count as i32).to_le_bytes().to_vec());
            data.extend(nt_str(&item)?);
            data.resize(36, 0); // Length should be exactly 36
            subrecords.push(Subrecord::new(*b"NPCO", data, false));
        }
        let header = RecordHeader::new(*b"CONT", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
