/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::util::{
    parse_float, push_opt_str_subrecord, push_str_subrecord, subrecords_len, take_nt_str,
    RecordError,
};
use derive_more::Constructor;
use esplugin::{RecordHeader, Subrecord};
use nom::number::complete::le_f32;
use nom_derive::Parse;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

/// A miscellaneous object
/// Can be carried, bought or sold
#[skip_serializing_none]
#[derive(DeltaRecord, FullRecord, Constructor, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Misc {
    /// In-game name of the object
    pub name: Option<String>,
    /// Model representing the object in-game
    pub model: Option<String>,
    /// Weight of the object
    pub weight: f64,
    /// Monetary value of the object
    pub value: u64,
    /// May indicate that this is a key.
    /// OpenMW documentation notes that there are many keys which
    /// do not have this set
    pub is_key: bool,
    /// Script for the object (run when?)
    pub script: Option<String>,
    /// Icon representing the object
    /// Displayed in the inventory (FIXME: Anywhere else?)
    pub icon: Option<String>,
}

impl TryFrom<esplugin::Record> for RecordPair<Misc> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let subrecords = record.subrecords();
        let mut id = None;
        let mut name = None;
        let mut model = None;
        let mut script = None;
        let mut icon = None;
        let mut misc_data = None;

        #[derive(Nom)]
        #[nom(LittleEndian)]
        struct Data {
            #[nom(Parse = "le_f32")]
            weight: f32,
            value: i32,
            is_key: i32,
        }

        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"FNAM" => name = Some(take_nt_str(data)?.1),
                b"MODL" => model = Some(take_nt_str(data)?.1),
                b"SCRI" => script = Some(take_nt_str(data)?.1),
                b"ITEX" => icon = Some(take_nt_str(data)?.1),
                b"MCDT" => {
                    misc_data = Some(Data::parse(data)?.1);
                }
                b"DELE" => (),
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }
        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;
        let data = misc_data.ok_or(RecordError::MissingSubrecord("MCDT"))?;
        let misc = Misc::new(
            name,
            model,
            parse_float(data.weight),
            data.value as u64,
            data.is_key != 0,
            script,
            icon,
        );
        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::Misc, id),
            misc,
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Misc> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        push_str_subrecord(&mut subrecords, &id, "NAME")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.name, "FNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.model, "MODL")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.script, "SCRI")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.icon, "ITEX")?;
        let mut data = vec![];
        data.extend(&(self.record.weight as f32).to_le_bytes());
        data.extend(&(self.record.value as i32).to_le_bytes());
        data.extend(&(self.record.is_key as i32).to_le_bytes());
        subrecords.push(Subrecord::new(*b"MCDT", data, false));
        let header = RecordHeader::new(*b"MISC", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
