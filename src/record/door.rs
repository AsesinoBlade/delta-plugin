/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::util::{
    push_opt_str_subrecord, push_str_subrecord, subrecords_len, take_nt_str, RecordError,
};
use esplugin::{RecordHeader, Subrecord};
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[skip_serializing_none]
#[derive(DeltaRecord, FullRecord, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Door {
    name: Option<String>,
    model: Option<String>,
    script: Option<String>,
    open_sound: Option<String>,
    close_sound: Option<String>,
}

impl TryFrom<esplugin::Record> for RecordPair<Door> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let subrecords = record.subrecords();
        let mut id = None;
        let mut name = None;
        let mut model = None;
        let mut script = None;
        let mut open_sound = None;
        let mut close_sound = None;

        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"FNAM" => name = Some(take_nt_str(data)?.1),
                b"MODL" => model = Some(take_nt_str(data)?.1),
                b"SCRI" => script = Some(take_nt_str(data)?.1),
                b"SNAM" => open_sound = Some(take_nt_str(data)?.1),
                b"ANAM" => close_sound = Some(take_nt_str(data)?.1),
                b"DELE" => (),
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }

        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;

        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::Door, id),
            Door {
                name,
                model,
                script,
                open_sound,
                close_sound,
            },
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Door> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        push_str_subrecord(&mut subrecords, &id, "NAME")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.name, "FNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.model, "MODL")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.script, "SCRI")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.open_sound, "SNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.close_sound, "ANAM")?;
        let header = RecordHeader::new(*b"DOOR", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
