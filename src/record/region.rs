/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::types::{take_rgba_colour, RGBAColour};
use crate::util::{
    byte_to_float, nt_str, push_opt_str_subrecord, push_str_subrecord, subrecords_len, take_nt_str,
    RecordError,
};
use derive_more::{Constructor, Display};
use esplugin::{RecordHeader, Subrecord};
use hashlink::LinkedHashMap;
use nom::number::complete::le_u8;
use nom::sequence::tuple;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[derive(Display, Clone, Debug, Hash, Eq, PartialEq, Serialize, Deserialize)]
pub enum WeatherType {
    Clear,
    Cloudy,
    Foggy,
    Overcast,
    Rain,
    Thunder,
    Ash,
    Blight,
    Snow,
    Blizzard,
}

#[derive(DeltaRecord, FullRecord, Constructor, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Region {
    name: Option<String>,
    weather: LinkedHashMap<WeatherType, f64>,
    // Identifier of a levelled list from which to take creatures that attack during sleep
    sleep_list: Option<String>,
    sounds: LinkedHashMap<String, f64>,
    map_colour: Option<RGBAColour>,
}

impl TryFrom<esplugin::Record> for RecordPair<Region> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let subrecords = record.subrecords();
        let mut id = None;
        let mut name = None;
        let mut sounds = LinkedHashMap::new();
        let mut weather = LinkedHashMap::new();
        let mut sleep_list = None;
        let mut map_colour = None;

        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"FNAM" => name = Some(take_nt_str(data)?.1),
                b"WEAT" => {
                    let (data, (clear, cloudy, foggy, overcast)) =
                        tuple((le_u8, le_u8, le_u8, le_u8))(data)?;
                    let (data, (rain, thunder, ash, blight)) =
                        tuple((le_u8, le_u8, le_u8, le_u8))(data)?;
                    insert_nonzero!(weather, WeatherType::Clear, byte_to_float(clear));
                    insert_nonzero!(weather, WeatherType::Cloudy, byte_to_float(cloudy));
                    insert_nonzero!(weather, WeatherType::Foggy, byte_to_float(foggy));
                    insert_nonzero!(weather, WeatherType::Overcast, byte_to_float(overcast));
                    insert_nonzero!(weather, WeatherType::Rain, byte_to_float(rain));
                    insert_nonzero!(weather, WeatherType::Thunder, byte_to_float(thunder));
                    insert_nonzero!(weather, WeatherType::Ash, byte_to_float(ash));
                    insert_nonzero!(weather, WeatherType::Blight, byte_to_float(blight));
                    // In data format 1.3 there may also be snow and blizzard
                    // There's no need to enforce esp version here, but we should account for both
                    if !data.is_empty() {
                        let (_, (snow, blizzard)) = tuple((le_u8, le_u8))(data)?;
                        insert_nonzero!(weather, WeatherType::Snow, byte_to_float(snow));
                        insert_nonzero!(weather, WeatherType::Blizzard, byte_to_float(blizzard));
                    }
                }
                b"BNAM" => sleep_list = Some(take_nt_str(data)?.1),
                b"CNAM" => {
                    map_colour = Some(take_rgba_colour(data)?.1);
                }
                b"SNAM" => {
                    let (_, sound_name) = take_nt_str(&data[0..32])?;
                    let (_, chance) = le_u8(&data[32..33])?;
                    sounds.insert(sound_name, byte_to_float(chance));
                }
                b"DELE" => (),
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }
        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;
        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::Region, id),
            Region::new(name, weather, sleep_list, sounds, map_colour),
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Region> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        push_str_subrecord(&mut subrecords, &id, "NAME")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.name, "FNAM")?;
        let mut data = vec![];
        use WeatherType::*;
        for weather_type in &[
            Clear, Cloudy, Foggy, Overcast, Rain, Thunder, Ash, Blight, Snow, Blizzard,
        ] {
            let chance = self.record.weather.get(weather_type).unwrap_or(&0.0);
            data.push((chance * 255.0).round() as u8);
        }
        subrecords.push(Subrecord::new(*b"WEAT", data, false));

        push_opt_str_subrecord(&mut subrecords, &self.record.sleep_list, "BNAM")?;

        if let Some(map_colour) = self.record.map_colour {
            subrecords.push(Subrecord::new(
                *b"CNAM",
                map_colour.to_le_bytes().to_vec(),
                false,
            ));
        }
        for (sound, chance) in self.record.sounds {
            let mut data = nt_str(&sound)?;
            data.resize(32, 0);
            data.push((chance * 255.0).round() as u8);
            subrecords.push(Subrecord::new(*b"SNAM", data, false));
        }

        let header = RecordHeader::new(*b"REGN", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
