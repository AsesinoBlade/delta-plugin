/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::effect::Effect;
use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::types::SpellType;
use crate::util::{
    is_default, push_opt_str_subrecord, push_str_subrecord, subrecords_len, take_nt_str,
    RecordError,
};
use esplugin::{RecordHeader, Subrecord};
use nom::number::complete::le_i32;
use num_traits::FromPrimitive;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[skip_serializing_none]
#[derive(FullRecord, DeltaRecord, Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct Spell {
    name: Option<String>,
    spell_type: SpellType,
    cost: u64,
    #[serde(default, skip_serializing_if = "is_default")]
    auto_calc: bool,
    #[serde(default, skip_serializing_if = "is_default")]
    pc_start: bool,
    #[serde(default, skip_serializing_if = "is_default")]
    always_succeeds: bool,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    #[multiset]
    effects: Vec<Effect>,
}

impl TryFrom<esplugin::Record> for RecordPair<Spell> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let subrecords = record.subrecords();

        let mut id = None;
        let mut name = None;
        let mut spell_type = None;
        let mut cost = 0;
        let mut auto_calc = false;
        let mut pc_start = false;
        let mut always_succeeds = false;
        let mut effects = vec![];

        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"FNAM" => name = Some(take_nt_str(data)?.1),
                b"SPDT" => {
                    let (data, i_spell_type) = le_i32(data)?;
                    spell_type = Some(
                        FromPrimitive::from_i32(i_spell_type)
                            .ok_or(RecordError::InvalidSpellTypeId(i_spell_type))?,
                    );
                    let (data, i_cost) = le_i32(data)?;
                    cost = i_cost;
                    let (_, flags) = le_i32(data)?;
                    auto_calc = (flags & 0x0001) != 0;
                    pc_start = (flags & 0x0002) != 0;
                    always_succeeds = (flags & 0x0004) != 0;
                }
                b"ENAM" => {
                    effects.push(data.try_into()?);
                }
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }

        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;
        let spell_type = spell_type.ok_or(RecordError::MissingSubrecord("DATA"))?;

        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::Spell, id.into()),
            Spell {
                name,
                spell_type,
                cost: cost as u64,
                auto_calc,
                pc_start,
                always_succeeds,
                effects,
            },
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Spell> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        push_str_subrecord(&mut subrecords, &id, "NAME")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.name, "FNAM")?;
        let mut data = vec![];
        data.extend(&(self.record.spell_type as u32).to_le_bytes());
        data.extend(&(self.record.cost as u32).to_le_bytes());
        let mut flags: u32 = 0;
        if self.record.auto_calc {
            flags |= 0x0001
        }
        if self.record.pc_start {
            flags |= 0x0002
        }
        if self.record.always_succeeds {
            flags |= 0x0004
        }
        data.extend(&flags.to_le_bytes());
        subrecords.push(Subrecord::new(*b"SPDT", data, false));
        for effect in self.record.effects {
            subrecords.push(effect.into());
        }

        let header = RecordHeader::new(*b"SPEL", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
