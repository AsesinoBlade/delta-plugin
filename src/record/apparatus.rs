/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::util::{
    parse_float, push_opt_str_subrecord, push_str_subrecord, subrecords_len, take_nt_str,
    RecordError,
};
use derive_more::Constructor;
use esplugin::{RecordHeader, Subrecord};
use nom::number::complete::le_f32;
use nom_derive::Parse;
use num_traits::FromPrimitive;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[derive(Copy, Clone, Debug, Eq, PartialEq, Serialize, Deserialize, FromPrimitive)]
pub enum ApparatusType {
    MortarPestle = 0,
    Alembic = 1,
    Calcinator = 2,
    Retort = 3,
}

/// An alchemical apparatus
#[skip_serializing_none]
#[derive(DeltaRecord, FullRecord, Constructor, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Apparatus {
    /// In-game name for the Apparatus
    pub name: Option<String>,
    /// Model identifier
    pub model: Option<String>,
    /// Icon identifier
    pub icon: Option<String>,
    /// Script (FIXME: When is the script run?)
    pub script: Option<String>,
    /// Type of apparatus
    pub apparatus_type: ApparatusType,
    /// Weight of the Apparatus
    pub weight: f64,
    /// Value of the Apparatus
    pub value: i64,
    /// Quality of the Apparatus
    pub quality: f64,
}

impl TryFrom<esplugin::Record> for RecordPair<Apparatus> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<RecordPair<Apparatus>, RecordError> {
        let subrecords = record.subrecords();
        let mut id = None;
        let mut name = None;
        let mut model = None;
        let mut script = None;
        let mut icon = None;
        let mut appa_data = None;

        #[derive(Nom)]
        #[nom(LittleEndian)]
        struct Data {
            typ: i32,
            #[nom(Parse = "le_f32")]
            quality: f32,
            #[nom(Parse = "le_f32")]
            weight: f32,
            value: i32,
        }

        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"FNAM" => name = Some(take_nt_str(data)?.1),
                b"MODL" => model = Some(take_nt_str(data)?.1),
                b"SCRI" => script = Some(take_nt_str(data)?.1),
                b"ITEX" => icon = Some(take_nt_str(data)?.1),
                b"AADT" => {
                    appa_data = Some(Data::parse(data)?.1);
                }
                b"DELE" => (),
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }

        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;
        let data = appa_data.ok_or(RecordError::MissingSubrecord("AADT"))?;
        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::Activator, id),
            Apparatus::new(
                name,
                model,
                icon,
                script,
                FromPrimitive::from_i32(data.typ)
                    .ok_or(RecordError::InvalidApparatusType(data.typ))?,
                parse_float(data.weight),
                data.value as i64,
                parse_float(data.quality),
            ),
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Apparatus> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        push_str_subrecord(&mut subrecords, &id, "NAME")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.name, "FNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.model, "MODL")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.icon, "ITEX")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.script, "SCRI")?;

        let mut data = vec![];
        data.extend(&(self.record.apparatus_type as i32).to_le_bytes());
        data.extend(&(self.record.quality as f32).to_le_bytes());
        data.extend(&(self.record.weight as f32).to_le_bytes());
        data.extend(&(self.record.value as i32).to_le_bytes());
        subrecords.push(Subrecord::new(*b"AADT", data, false));

        let header = RecordHeader::new(*b"APPA", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
